import * as Sentry from "@sentry/node";
import { ProfilingIntegration } from "@sentry/profiling-node";
import express from 'express';
import cors from 'cors';

import userRouter from "./routes/users.routes.js";
import projectRouter from './routes/projects.routes.js';
import ticketRouter from './routes/tickets.routes.js';

import { 
  getStatusCode,
} from './utils/statusCode.js';

const app = express();

Sentry.init({
  dsn: 'https://9023bf690f25b07e1eb4e21135b655d3@o4505993654239232.ingest.sentry.io/4505993657384960',
  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new Sentry.Integrations.Express({ app }),
    new ProfilingIntegration(),
  ],
  // Performance Monitoring
  tracesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
  // Set sampling rate for profiling - this is relative to tracesSampleRate
  profilesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
});

// Middlewares.
app.use(cors());
app.use(express.json());
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());

// Route to know if my API is running
app.get('/ping', (_req, res) => {
  res.status(200).json({
    message: 'The API is running'
  });
});

// Routes.
app.use('/users', userRouter);
app.use('/projects', projectRouter);
app.use('/tickets', ticketRouter);

app.use(Sentry.Handlers.errorHandler());

// Middleware errors.
app.use((err, _req, res, _next) => {  // eslint-disable-line no-unused-vars
  let statusCode = getStatusCode(err.message);
  res.status(statusCode).json({ message: err.message });
});

export default app;