import mongoose from "mongoose";
import 'dotenv/config';

const { DB_NAME, MON_LOC_DB, MON_LOC_RED, MONGO_TYPE_CONNECT } = process.env;

const MONGO_DB_UB = MONGO_TYPE_CONNECT === 'red' ?
    MON_LOC_RED
    :
    MON_LOC_DB

mongoose.connect(`mongodb://${MONGO_DB_UB}/${DB_NAME}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.on('error', () => {
  console.error('Error de conexion con la base de datos.')
});

mongoose.connection.once('open', () => {
  console.log('MongoDB: Conexión exitosa con la base de datos.');
});

export default mongoose;
