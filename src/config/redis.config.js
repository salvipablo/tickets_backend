import redis from 'redis';
import 'dotenv/config';

let redisClient;

(async () => {
  const REDIS_URI = process.env.REDIS_TYPE_CONNECT === 'red' ? 
      process.env.REDIS_URI_RED 
      :
      process.env.REDIS_URI_LOC;

  redisClient = redis.createClient({ url: REDIS_URI });

  await redisClient.connect();
})();

export default redisClient;