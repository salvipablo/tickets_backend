import jwt from 'jsonwebtoken';
import 'dotenv/config';

import {
  getStatusCode
} from '../utils/statusCode.js';

const { TOKEN_SECRET } = process.env;

import {
  allProjects,
  add,
} from "../models/projects.model.js";

export const getAllProjects = async (req, res, next) => {
  try {
    const decoded = jwt.verify(req.token, TOKEN_SECRET);

    let operationAllProjects = await allProjects(decoded.id);
  
    let statusCode = getStatusCode(operationAllProjects.message);
  
    return res.status(statusCode).json({
      message: operationAllProjects.message,
      data: operationAllProjects.data
    });
  } catch (error) {
    next(error);
  }
};

export const createProjects = async (req, res, next) => {
  try {
    const decoded = jwt.verify(req.token, TOKEN_SECRET);

    let dataProject = {
      nameProject: req.body.nameProject,
      description: req.body.description,
      idUser: decoded.id
    }

    let operationCreateProject = await add(dataProject);

    let statusCode = getStatusCode(operationCreateProject.message);

    return res.status(statusCode).json({
      message: operationCreateProject.message,
      data: operationCreateProject.data
    });
  } catch (error) {
    next(error);
  }
};
