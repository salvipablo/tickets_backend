import url from "url";

import {
  getStatusCode
} from '../utils/statusCode.js';

import {
  add,
  allTickets,
} from "../models/tickets.model.js";

export const createTicket = async (req, res, next) => {
  try {
    const parsedUrl = url.parse(req.url, true);

    const idProject = parsedUrl.query.idProject;
  
    let operationCreate = await add(req.body, req.file.path, idProject);
  
    let statusCode = getStatusCode(operationCreate.message);
  
    return res.status(statusCode).json({
      message: operationCreate.message,
      data: operationCreate.data
    });
  } catch (error) {
    next(error);
  }
};

export const getAllTickets = async (req, res, next) => {
  try {
    const parsedUrl = url.parse(req.url, true);

    const idProject = parsedUrl.query.idProject;

    let allTicketsSearch = await allTickets(idProject);

    let statusCode = getStatusCode(allTicketsSearch.message);

    return res.status(statusCode).json({
      message: allTicketsSearch.message,
      data: allTicketsSearch.data
    });
  } catch (error) {
    next(error);
  }
};
