import jwt from 'jsonwebtoken';
import 'dotenv/config';

import {
  getStatusCode
} from '../utils/statusCode.js';

const { TOKEN_SECRET } = process.env;

import {
  allUsers,
  erase,
  login,
  register,
  profile,
  editProfile
} from "../models/users.model.js";

export const getAllUsers = async (_req, res, next) => {
  try {
    let operationAllUsers = await allUsers();

    let statusCode = getStatusCode(operationAllUsers.message);
  
    return res.status(statusCode).json({
      message: operationAllUsers.message,
      data: operationAllUsers.data
    });
  } catch (error) {
    next(error);
  }
};

export const deleteUser = async (req, res, next) => {
  try {
    let operationDelete = await erase(req.params.id);

    let statusCode = getStatusCode(operationDelete.message);
  
    return res.status(statusCode).json({
      message: operationDelete.message,
      data: operationDelete.data
    });
  } catch (error) {
    next(error);
  }
};

export const registerUser = async (req, res, next) => {
  try {
    let operationRegister = await register(req.userToRegister);

    let statusCode = getStatusCode(operationRegister.message);
  
    return res.status(statusCode).json({
      message: operationRegister.message,
      data: operationRegister.data
    });
  } catch (error) {
    next(error);
  }
};

export const loginUser = async (req, res, next) => {
  try {
    let operationLogin = await login(req.userToLogin);

    let statusCode = getStatusCode(operationLogin.message);
  
    return res.status(statusCode).json({
      message: operationLogin.message,
      data: operationLogin.data
    });
  } catch (error) {
    next(error);
  }
};

export const getProfile = async (req, res, next) => {
  try {
    const decoded = jwt.verify(req.token, TOKEN_SECRET);

    let operationGetProfile = await profile(decoded.id);
  
    let statusCode = getStatusCode(operationGetProfile.message);
  
    return res.status(statusCode).json({
      message: operationGetProfile.message,
      data: operationGetProfile.data
    });
  } catch (error) {
    next(error);
  }
};

export const profileEdit = async (req, res, next) => {
  try {
    const decoded = jwt.verify(req.token, TOKEN_SECRET);

    let operationEditProfile = await editProfile(decoded.id, req.body);
  
    let statusCode = getStatusCode(operationEditProfile.message);
  
    return res.status(statusCode).json({
      message: operationEditProfile.message
    });
  } catch (error) {
    next(error);
  }
};