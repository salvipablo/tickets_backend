import 'dotenv/config';

import app from './app.js';
import _redisClient from './config/redis.config.js';  // eslint-disable-line no-unused-vars

const { PORT } = process.env;

// Server start.
app.listen(PORT, () => {
  try {
    console.clear();

    console.log(`Server listening on port: ${PORT}`);
  } catch (error) {
    console.log(`Server unable to start. Failed with the following message: ${error.message}`);
  }
});
