import Joi from "joi";

const schemaLogin = Joi.object(
  {
    username: Joi.string()
    .trim()
    .min(6)
    .max(30)
    .alphanum()
    .required(),

    password: Joi.string()
    .min(8)
    .max(30)
    .alphanum()
    .required()
  }
);

export const validationLogin = (req, res, next) => {
  const { error, value } = schemaLogin.validate(req.body)

  if (error) throw new Error('Validations have not been passed');

  req.userToLogin = value;

  next();
};
