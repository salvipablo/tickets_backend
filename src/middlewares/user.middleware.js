import Joi from "joi";
import url from "url";

const schemaCreateUser = Joi.object(
  {
    firstname: Joi.string()
    .trim()
    .required(),

    lastname: Joi.string()
    .trim()
    .required(),

    email: Joi.string()
    .trim()
    .email()
    .required(),

    username: Joi.string()
    .trim()
    .min(6)
    .max(30)
    .alphanum()
    .required(),

    password: Joi.string()
    .min(8)
    .max(30)
    .alphanum()
    .required(),
  }
);

export const validationRegister = (req, _res, next) => {
  let dataValidate = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
  };

  const { error } = schemaCreateUser.validate(dataValidate)

  if (error) throw new Error('Validations have not been passed');

  dataValidate.userLevel = 2;
  dataValidate.active = true;

  req.userToRegister = dataValidate;

  next();
};

export const validationToken = (req, _res, next) => {
  const parsedUrl = url.parse(req.url, true);

  const token = parsedUrl.query.token;

  if (!token) throw new Error('You are not authorized to make this request');

  req.token = token;

  next();
};
