import mongoose from "../config/mongo.config.js";

const projectSchema = new mongoose.Schema(
  {
    nameProject: String,
    description: String,
    idUser: String
  }, {
    timestamps: true
  }
);

const Project = mongoose.model("projects", projectSchema);

export default Project;
