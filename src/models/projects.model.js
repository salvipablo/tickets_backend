import Project from "./project.model.js";

export const allProjects = async (idUser) => {
  let projects = await Project.find({ idUser });

  if (projects.length === 0) {
    throw new Error('No projects were found in the database.')
  }

  return {
    message: 'Sending of all projects of the database',
    data: projects
  };
};

export const add = async (dataForProject) => {
  let createProject = await Project.create(dataForProject);

  return {
    message: 'Project created successfully',
    data: createProject
  };
};
