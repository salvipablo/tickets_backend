import mongoose from "../config/mongo.config.js";

const ticketSchema = new mongoose.Schema(
  {
    nameTicket: String,
    description: String,
    idTicket: String,
    idProject: String,
    urlPhoto: String,
  }, {
    timestamps: true
  }
);

const Ticket = mongoose.model("tickets", ticketSchema);

export default Ticket;
