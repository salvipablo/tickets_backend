import Ticket from "./ticket.model.js";
import {v2 as cloudinary} from 'cloudinary';

cloudinary.config({ 
  cloud_name: 'denqnt687', 
  api_key: '849378987999278', 
  api_secret: '95TLu7lfXPpta3NJ2sy3ax3FqDs' 
});

export const add = async (dataBody, pathFile, idProject) => {
  let imagePath;

  await cloudinary.uploader.upload(pathFile, (error, result) => {
    if (!error) imagePath = result.secure_url
  });

  let dataForTicket = {
    nameTicket: dataBody.ticketName,
    description: dataBody.ticketDescription,
    idTicket: dataBody.idTicket,
    idProject: idProject,
    urlPhoto: imagePath,
  }

  let createTicket = await Ticket.create(dataForTicket);

  return {
    message: 'Ticket created successfully',
    data: createTicket
  };
};

export const allTickets = async (idProject) => {
  const tickets = await Ticket.find({ idProject });

  if (tickets.length === 0) throw new Error('No tickets found for this project');

  return {
    message: 'Sending all tickets',
    data: tickets
  };
};
