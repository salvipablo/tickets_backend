import bcrypt from 'bcrypt';
import 'dotenv/config';
import jwt from 'jsonwebtoken';

import redisClient from '../config/redis.config.js';

import User from "./user.model.js";

const { TOKEN_SECRET } = process.env;

export const allUsers = async () => {
  let users = await User.find({});

  if (users.length === 0) {
    throw new Error('No users were found in the database.')
  }

  return {
    message: 'Sending of all users of the database.',
    data: users
  };
};

export const erase = async (idUser) => {
  let deleteUser = await User.findByIdAndDelete({ _id: idUser });

  if (!deleteUser) {
    throw new Error('The user you are trying to delete does not exist in the database');
  }

  return {
    message: 'User successfully deleted',
    data: deleteUser
  };
};

export const register = async (dataForRegister) => {
  dataForRegister.password = await encryptPassword(dataForRegister.password);

  let registerUser = await User.create(dataForRegister);

  return {
    message: 'User register successfully',
    data: registerUser
  };
};

export const login = async (dataForLogin) => {
  const { username, password } = dataForLogin;

  let login = await User.findOne({ username });

  if (!login) {
    throw new Error('The user trying to log in does not exist in the database');
  }

  if (!await bcrypt.compare(password, login.password)) {
    throw new Error('The password entered is incorrect');
  }

  let infoUserForToken = {
    id: login._id,
    username: login.username,
    active: login.active,
    userLevel: login.userLevel
  };

  const token = jwt.sign(
    infoUserForToken,
    TOKEN_SECRET
  );

  return {
    message: 'Permission granted',
    data: token
  };
};

export const profile = async (idUser) => {
  let user = await User.findOne({ _id: idUser }).select('firstname lastname email username');

  if (!user) throw new Error('Profile or user not found');

  const userCache = JSON.stringify({
    firstname: user.firstname,
    lastname: user.lastname,
    email: user.email,
    username: user.username
  });

  let dataResponse = {};

  let data = await redisClient.get(idUser);

  if (!data) {
    redisClient.set(idUser, userCache, {EX: parseInt(process.env.REDIS_TTL) });
    dataResponse = JSON.parse(userCache)
  } else {
    dataResponse = JSON.parse(data);
  }

  return {
    message: 'Sending profile information',
    data: dataResponse
  };
};

export const editProfile = async (idUser, dataUpdateForUser) => {
  const updateUser = await User.findByIdAndUpdate({ _id: idUser }, dataUpdateForUser);

  if (!updateUser) throw new Error('Profile or user not found');

  redisClient.del(idUser);

  return {
    message: 'User profile edited successfully'
  };
};

const encryptPassword = async (password) => {
  const salt = bcrypt.genSaltSync(10);
  const passwordHashed = await bcrypt.hash(password, salt);
  return passwordHashed;
};