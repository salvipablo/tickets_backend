import express from "express";

import {
  getAllProjects,
  createProjects,
} from "../controllers/projects.controller.js";

import {
  validationToken,
} from "../middlewares/user.middleware.js";

const projectRouter = express.Router();

projectRouter.get('/', validationToken, getAllProjects);
projectRouter.post('/', validationToken, createProjects);

export default projectRouter;