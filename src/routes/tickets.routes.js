import express from "express";

import {
  createTicket,
  getAllTickets,
} from "../controllers/tickets.controller.js";

import {
  validationToken,
} from "../middlewares/user.middleware.js";

import upload from "../middlewares/multer.middleware.js";

const ticketRouter = express.Router();

ticketRouter.post('/', validationToken, upload.single('photo'), createTicket);
ticketRouter.get('/', validationToken, getAllTickets);

export default ticketRouter;