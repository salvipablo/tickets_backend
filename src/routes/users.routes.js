import express from "express";

import {
  getAllUsers,
  deleteUser,
  loginUser,
  registerUser,
  getProfile,
  profileEdit,
} from "../controllers/users.controller.js";

import {
  validationRegister,
  validationToken,
} from "../middlewares/user.middleware.js";

import {
  validationLogin,
} from "../middlewares/login.middleware.js";

const userRouter = express.Router();

userRouter.get('/', getAllUsers);
userRouter.delete('/:id', deleteUser);

userRouter.post('/register/', validationRegister, registerUser);
userRouter.post('/login/', validationLogin, loginUser);

userRouter.get('/profile/', validationToken ,getProfile);
userRouter.put('/profile/', validationToken, profileEdit);

export default userRouter;
