import app from "../app";
import request from "supertest";

describe('GET /ping', () => {
  test('Verifying that the API is running', async () => {
    const response = await request(app).get('/ping').send();
    expect(response.status).toBe(200);
  });
});

describe('We start with the empty user database - Endpoint: (/users) - Checking status codes', () => {
  test('Request to obtain all users, I expect status code 404', async () => {
    const response = await request(app).get('/users').send();
    expect(response.status).toBe(404);
  });

  test('Request to delete a user, waiting for status code 404', async () => {
    const response = await request(app).delete('/users/651d78ff4e6884d50165b7d7').send();
    expect(response.status).toBe(404);
  });

  test('Get user profile, hope to get 404', async () => {
    const response = await request(app).get('/users/profile?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1MWRhMTc1ZjE3MDVlMTU4M2QxYTcwMCIsInVzZXJuYW1lIjoicHNhbHZpIiwiYWN0aXZlIjp0cnVlLCJ1c2VyTGV2ZWwiOjIsImlhdCI6MTY5NjQ0Nzk0M30.grDcsVFBm7NInTgqGcQnG9zNdKCdWOh9KhNlp_QRpK4').send();
    expect(response.status).toBe(404);
  });

  test('Edit user profile, hope to get 404', async () => {
    const response = await request(app).put('/users/profile?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1MWRhMTc1ZjE3MDVlMTU4M2QxYTcwMCIsInVzZXJuYW1lIjoicHNhbHZpIiwiYWN0aXZlIjp0cnVlLCJ1c2VyTGV2ZWwiOjIsImlhdCI6MTY5NjQ0Nzk0M30.grDcsVFBm7NInTgqGcQnG9zNdKCdWOh9KhNlp_QRpK4').send();
    expect(response.status).toBe(404);
  });

  // TODO: Cases where the different validations are not passed should be considered.

  // Correct case where we know that the user meets all validations ** Register **.
    let newUser = {
      firstname: "Pablo",
      lastname: "Salvi",
      email: "psalvi@test.com.ar",
      username: "psalvi",
      password: "123456789"
    };

    test('Register new user, Im waiting for a code 201', async () => {
      const response = await request(app).post('/users/register/').send(newUser);
      expect(response.status).toBe(201);
    });
  // Correct case where we know that the user meets all validations  ** Register **.

  // Correct case where we know that the user meets all validations ** Login **.
    let userToLogin = {
      username: 'psalvi',
      password: '123456789'
    };

    test('User login, waiting for a status code 201', async () => {
      const response = await request(app).post('/users/login/').send(userToLogin);
      expect(response.status).toBe(201);
    });
    // Correct case where we know that the user meets all validations ** Login **.
});
