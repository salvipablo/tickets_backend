const statusCode = {
  'Sending of all users of the database.': 200 ,
  'No users were found in the database.': 404,
  'User created successfully': 200,
  'The user you are trying to delete does not exist in the database': 404,
  'User successfully deleted': 200,
  'User register successfully': 201,
  'The user trying to log in does not exist in the database': 404,
  'The password entered is incorrect': 403,
  'Permission granted': 201,
  'Sending profile information': 200,
  'User profile edited successfully': 200,
  'No projects were found in the database.': 404,
  'Sending of all projects of the database': 200,
  'Project created successfully': 201,
  'Ticket created successfully': 201,
  'No tickets found for this project': 404,
  'Sending all tickets': 200,
  'You are not authorized to make this request': 403,
  'Validations have not been passed': 401,
  'Profile or user not found': 404
};

export function getStatusCode(message) {
  return !statusCode[message] ? 500 : statusCode[message];
}
