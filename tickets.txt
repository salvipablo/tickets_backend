Tickets for new features.
T#001 - Project start.                                                  - OK.
T#002 - Users.                                                          - OK.
T#003 - Redis.                                                          - OK.
T#004 - Update profile.                                                 - OK.
T#005 - Projects.                                                       - OK.
T#006 - Tickets.                                                        - OK.
T#007 - Ticket list.                                                    - OK.
T#008 - Cloudinary.                                                     - OK.
T#009 - Sentry.                                                         - OK.
T#010 - Mailing.                                                        - 
T#011 - Jest.                                                           - OK.
T#012 - Kafka.                                                          - 
T#013 - Eslint.                                                         - OK.


Tickets for code refactorings.
R#001 - Cache.                                                          - OK.
R#002 - Mongo.                                                          - OK.
R#003 - not .env.                                                       - OK.
R#004 - Object status error codes.                                      - OK.
R#005 - Errors.                                                         - OK.
R#006 - Jest.                                                           - OK.

Tickets to correct errors.
B#001 - 